package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);

		ApitechuApplication.productModels = ApitechuApplication.getTestData();

	}

	private static ArrayList<ProductModel> getTestData() {

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						,"Producto 1"
						,10
				)
		);

		productModels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,20
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						,"Producto 3"
						,30
				)
		);

		return productModels;
	}
}
